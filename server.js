const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.urlencoded({
  extended: true,
}));

app.use(bodyParser.json());

app.use(cors());

const dbConfig = require('./config/database.config');
const mongoose = require('mongoose');

mongoose.connect(dbConfig.url, {
  useNewUrlParser: true,
})
  .then(() => console.log('Connection to DB was established'))
  .catch(err => {
    console.error('Could not connect to DB', err);
    process.exit();
  });

app.get('/', (req, res) => {
  res.json({
    message: "Welcome",
  });
});

require('./app/routes/user.routes')(app);

app.listen(3000, () => {
  console.log('Connection established. Server is listening on port 3000');
});
