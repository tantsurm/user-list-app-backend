const mongoose = require('mongoose');

const UserScheme = mongoose.Schema({
  name: String,
  email: String,
  address: String,
  age: {
    type: Number,
    min: 18,
    max: 120
  },
}, {
  timestamps: true,
});

module.exports = mongoose.model('User', UserScheme);
