const User = require('../models/user.model');

exports.create = ({
  body
}, res) => {

  if (!body.name) {
    return res.status(400).send({
      message: 'User name can\'t be empty',
    })
  }

  const user = new User({
    name: body.name,
    email: body.email || null,
    address: body.address || null,
    age: body.age || null,
  });

  user.save()
    .then(data => res.send(data))
    .catch(err => res.status(500).send({
      message: err.message || "Some error occurred while creating the user",
    }))
};

exports.findAll = (req, res) => {
  User.find()
    .then(notes => res.send(notes))
    .catch(err => res.status(500).send({
      message: err.message || "Some error occurred while getting the user from DB",
    }))
};

exports.findOne = (req, res) => {
  const userId = req.params.userId;

  User.findById(userId)
    .then(user => res.send(user))
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: `User with id of ${userId} is not found in the DB`,
        });
      }

      return res.status(500).send({
        message: "Something went wrong",
      });
    })
};

exports.update = ({
  body,
  params
}, res) => {

  if (body.age < 18 || body.age > 120) {
    return res.status(400).send({
      message: 'User age is invalid',
    })
  }

  const userId = params.userId;

  User.findByIdAndUpdate(userId, body, {
      new: true
    })
    .then(user => res.send(user))
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: `User with id of ${userId} is not found in the DB`,
        });
      }

      return res.status(500).send({
        message: "Something went wrong while updating user",
      });
    })
};

exports.delete = (req, res) => {
  const userId = req.params.userId;

  User.findByIdAndDelete(userId)
    .then(() => res.send({
      message: "User deleted successfully!"
    }))
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: `User with id of ${userId} is not found in the DB`,
        });
      }

      return res.status(500).send({
        message: "Something went wrong while deleting user"
      });
    })
};
